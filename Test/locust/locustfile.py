import time
from locust import HttpUser, task, between
from locust.runners import MasterRunner, WorkerRunner

class QuickstartUser(HttpUser):
    wait_time = between(1, 2.5)

    @task
    def hello_world(self):
        self.client.verify = False
        self.client.get("/")     

    @task(7)
    def vector_attacks(self):
        attack = [ '/?exec=/bin/bash' , 
                   '/?q="><script>alert(1)</script>"' , 
                   "/?id=3 or 'a'='a'" ]
        for command in attack:
            self.client.get(command)
            time.sleep(1)

