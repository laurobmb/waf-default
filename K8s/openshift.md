# Openshift

Comandos para deploy no openshift v4.6, a sequencia de comandos cria dois projetos distintos para testar a administracao de recursos da plataforma

## Projeto 1
    oc new-project waf-1
    oc new-app --name web-application-firewall-1 --labels=app=waf-1 --env=BACKEND=181.215.183.135 quay.io/lagomes/waf:main
    oc create serviceaccount waf-sa
    oc adm policy add-scc-to-user anyuid -z waf-sa
    oc adm policy add-role-to-user edit -z waf-sa
    oc set serviceaccount deployment/web-application-firewall-1 waf-sa
    oc create route passthrough web-application-firewall-tls-1 --port 8443-tcp --service web-application-firewall-1 --hostname conectarecife.recife.pe.gov.br
    oc set resources deployment web-application-firewall-1 --limits=cpu=400m,memory=512Mi --requests=cpu=100m,memory=256Mi
    oc set probe deployment/web-application-firewall-1 --readiness --initial-delay-seconds=30 --timeout-seconds=5 --get-url=https://:8443/_v/healthcheck 
    oc set probe deployment/web-application-firewall-1 --liveness --initial-delay-seconds=30 --timeout-seconds=5 --get-url=https://:8443/_v/healthcheck
    oc autoscale deployment web-application-firewall-1 --max 50 --min 3 --cpu-percent=80

## Projeto 2
    oc new-project waf-2
    oc new-app --name web-application-firewall-2 --labels=app=waf-2 --env=BACKEND=201.182.53.24 quay.io/lagomes/waf:main
    oc create serviceaccount waf-sa
    oc adm policy add-scc-to-user anyuid -z waf-sa
    oc adm policy add-role-to-user edit -z waf-sa
    oc set serviceaccount deployment/web-application-firewall-2 waf-sa
    oc create route passthrough web-application-firewall-tls-2 --port 8443-tcp --service web-application-firewall-2 --hostname www.trf5.jus.br
    oc set resources deployment web-application-firewall-2 --limits=cpu=400m,memory=512Mi --requests=cpu=100m,memory=256Mi
    oc set probe deployment/web-application-firewall-2 --readiness --initial-delay-seconds=30 --timeout-seconds=5 --get-url=https://:8443/_v/healthcheck 
    oc set probe deployment/web-application-firewall-2 --liveness --initial-delay-seconds=30 --timeout-seconds=5 --get-url=https://:8443/_v/healthcheck
    oc autoscale deployment web-application-firewall-2 --max 50 --min 3 --cpu-percent=80

## Projeto 3
    oc new-project waf-3
    oc new-app --name web-application-firewall-3 --labels=app=waf-3 --env=BACKEND=192.0.66.200 https://gitlab.com/laurobmb/waf-default#main --context-dir ProxyReverse
    oc create serviceaccount waf-sa
    oc adm policy add-scc-to-user anyuid -z waf-sa
    oc adm policy add-role-to-user edit -z waf-sa
    oc set serviceaccount deployment/web-application-firewall-3 waf-sa
    oc create route passthrough web-application-firewall-tls-3 --port 8443-tcp --service web-application-firewall-3 --hostname www.cnnbrasil.com.br
    oc set resources deployment web-application-firewall-3 --limits=cpu=400m,memory=512Mi --requests=cpu=100m,memory=256Mi
    oc set probe deployment/web-application-firewall-3 --readiness --initial-delay-seconds=30 --timeout-seconds=5 --get-url=https://:8443/_v/healthcheck 
    oc set probe deployment/web-application-firewall-3 --liveness --initial-delay-seconds=30 --timeout-seconds=5 --get-url=https://:8443/_v/healthcheck
    oc autoscale deployment web-application-firewall-3 --max 50 --min 3 --cpu-percent=80

## Testes

Para testar a plataforma usei o DNSmasq para mudar as consultas de DNS da minha maquina de testes, e encaminhei os pacotes para os routers default do openshift exemplo de rota criada [router.yaml](route.yaml)



