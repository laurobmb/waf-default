from datetime import datetime
DATE = datetime.today().strftime('%Y-%m-%d %H:%M')
writepath = '/etc/nginx/modsecurity/crs-setup.conf'
print("# Configurando nivel de paranoia #")
paranoia_level = 'SecAction "id:900000,phase:1,nolog,pass,t:none,setvar:tx.paranoia_level=1"'
with open(writepath, "a") as file_object:
    file_object.write('### Editado ao iniciar '+DATE+' #### \n')
    file_object.write(paranoia_level+'\n')
print(paranoia_level,"no arquivo",writepath)    
print("# Configurando nivel de paranoia #\n")

