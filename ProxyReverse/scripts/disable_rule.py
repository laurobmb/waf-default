from datetime import datetime

DATE = datetime.today().strftime('%Y-%m-%d %H:%M')

writepath = '/etc/modsecurity.d/owasp-crs/crs-setup.conf'
rules=[954011] # regra de exemplo, deve existir um valor aqui #OBS o numero da regra deve estar correto, caso não esteja o nginx não vai iniciar

print("########## Removendo regras do catalogo de rules do CRS ##########")
print('########## File is '+writepath+' ##########')

# Descrição das regras 
# 954011 - END-RESPONSE-954-DATA-LEAKAGES-IIS
# 949110 - TX:ANOMALY_SCORE
# 959100 - TX:OUTBOUND_ANOMALY_SCORE
# rules=[949110,954011,959100]

with open(writepath, "a") as file_object:
    file_object.write('### Editado ao iniciar '+DATE+' #### \n')
    for i in rules:
        print('removendo regra número -> ',str(i))
        file_object.write('SecRuleRemoveById '+str(i)+'\n')

print('########## Removendo regras do catalogo de rules do CRS ##########\n')

