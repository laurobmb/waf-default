#!/bin/bash

echo "Criando certificado auto assinado"
openssl req -x509 -sha256 -newkey rsa:2048 -keyout /root/.certs/certificate.key -out /root/.certs/certificate.crt -days 1024 -nodes -subj "/C=BR/ST=Pernambuco/L=Recife/O=Decript Cyber security /O=DECRIPT/CN=waf.decript.ai"
echo "Deletando regras desnecessárias"
python /delete_rule.py 
echo "Desabilitando regras desnecessárias"
python /disable_rule.py
echo "Alterando backend de conexão"
sed -i s/BACKEND/$BACKEND/g /etc/nginx/conf.d/default.conf

if nginx -t; then
	nginx -g 'daemon off;'
else
	echo "Erro !!!!"
fi


