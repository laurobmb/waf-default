# WAF

[Reference](https://gitlab.com/laurobmb/modelo-waf-default)

![modsecurity](Imagens/modsecurity.png)

## Teste blocks no WAF
* https://m.DOMAIN.com.br/.svn/entries
* https://m.DOMAIN.com.br/test?id=1%20and%201=1
* https://www.DOMAIN.com.br/.svn/entries
* https://www.DOMAIN.com.br/test?id=1%20and%201=1
* https://DOMAIN.com.br/.svn/entries
* https://DOMAIN.com.br/test?id=1%20and%201=1

## Novos deploys são explicados na pasta do ansible
[ANSIBLE](..ansible/README.md)

# SECURITY adivisor !!!! you should change ssh key on .ssh directory

    ssh-keygen -C "modsec-source-builder/repo@gitlab" -f modsec-repo -N ''
